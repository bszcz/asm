; Copyright (c) 2012 Bartosz Szczesny
; LICENSE: The MIT License (MIT)

section .data
	msg	db "Hello World!", 10, 0	; 10 = '\n', 0 = '\0'

section .text
	global _start
	_start:

		mov ecx, msg	; load before calling print_msg
		call print_msg

		mov eax, 1	; __NR_exit
		mov ebx, 0	; return value
		int 0x80	; kernel interrupt

	print_msg:
		pusha		; save all registers
		push ecx	; save location
		mov edx, 0	; zero counter

		.loop:				; '.' means local to print_msg
			cmp byte [ecx], 0	; is character == '\0' (compare 1 byte)
			je .finish		; if so then quit loop
			inc edx			; increase counter
			inc ecx			; increase string address
		jmp .loop			; process next character

		.finish:
			mov eax, 4	; __NR_write
			mov ebx, 1	; stdout
			pop ecx		; get saved location
			int 0x80	; kernel interrupt

		popa	; load all registers
		ret	; return
