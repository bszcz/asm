; Copyright (c) 2012 Bartosz Szczesny
; LICENSE: The MIT License (MIT)

section .data
	msg	db "Type up to 7 characters, hit enter.", 10	; 10 = "\n"
	len	equ $ - msg	; lenght = current address ("$") - address of "msg"

section .bss
	buf resb 8	; reserve 8 byte buffer, put address to "buf"
			; value of 'enter/return' key is stored as well

section .text
	global _start

	_start:

		; write welcome msg
		mov eax, 4		; __NR_write
		mov ebx, 1		; write to STDOUT
		mov ecx, msg		; write from buffer
		mov edx, len		; write len bytes
		int 0x80		; kernel interrupt

		; read to buffer
		mov eax, 3		; __NR_read
		mov ebx, 0		; read from STDIN
		mov ecx, buf		; read into buffer
		mov edx, 8		; read 8 bytes
		int 0x80		; kernel interrupt

		; write from buffer
		mov eax, 4		; __NR_write
		mov ebx, 1		; write to STDOUT
		mov ecx, buf		; write from buffer
		mov edx, 8		; write 8 bytes
		int 0x80		; kernel interrupt

		; exit
		mov eax, 1		; __NR_exit
		mov ebx, 0		; return value
		int 0x80		; kernel interrupt
