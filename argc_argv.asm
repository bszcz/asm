; Copyright (c) 2012 Bartosz Szczesny
; LICENSE: The MIT License (MIT)

section .data
	new_line	db  10,0	; "\n\0"

section .text
	global _start

	_start:
		pop edx				; pop number of arguments

		.printarg:
			pop ecx			; pop argument from stack
			call print_msg		; print argument string
			mov ecx, new_line	; load new line string
			call print_msg		; print new line
			dec edx			; decrease argument counter
			cmp edx, 0		; check if zero arguments left
			jne .printarg		; if not zero then repeat loop

		mov eax, 1	; __NR_exit
		mov ebx, 0	; return value
		int 80h		; kernel interrupt

	print_msg:
		pusha		; save all registers
		push ecx	; save location
		mov edx, 0	; zero counter

		.strlen:			; '.' means local to print_msg
			cmp byte [ecx], 0	; is character == '\0' (compare 1 byte)
			je .finish		; if so then quit loop
			inc edx			; increase counter
			inc ecx			; increase string address
		jmp .strlen			; process next character

		.finish:
			mov eax, 4	; __NR_write
			mov ebx, 1	; stdout
			pop ecx		; get saved location
			int 0x80	; kernel interrupt

		popa	; load all registers
		ret	; return
