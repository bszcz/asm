# Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
# This program is free software under the MIT license.

# $ gcc -nostdlib hello_world_x64.s

msg:
	.ascii  "Hello world.\n"

.global _start
_start:
	# write(stdout, msg, strlen(msg))
	mov $1,   %rax # 1  is write()
	mov $1,   %rdi # 1  is stdout
	mov $msg, %rsi
	mov $13,  %rdx # 13 is strlen(msg)
	syscall

	# exit(0)
	mov $60, %rax # 60 is exit()
	mov $0,  %rdi # 0  is exit code
	syscall
