; Copyright (c) 2012 Bartosz Szczesny
; LICENSE: The MIT License (MIT)

; call like this: cat file.txt | a.out
; could also XOR buffer for encryption

section .data
	bufsz	equ 32		; size of the buffer

section .bss
	buf	resb bufsz	; reserve "bufsz" bytes for buffer

section .text
	global _start
	_start:

		.streamloop:
			; read to buffer
			mov eax, 3		; __NR_read
			mov ebx, 0		; read from STDIN
			mov ecx, buf		; read into buffer
			mov edx, bufsz		; read 'bufsz' bytes
			int 0x80		; kernel interrupt

			push eax		; save number of read bytes
			mov edx, eax		; and print only as many

			; write from buffer
			mov eax, 4		; __NR_write
			mov ebx, 1		; write to STDOUT
			mov ecx, buf		; write from butter
			int 0x80		; kernel interrupt

			pop eax
			cmp eax, bufsz		; read returns number of read bytes
			jb .streamend		; if less than 'bufsz' then quit
			jmp .streamloop		; if not do another loop

		.streamend:
		mov eax, 1	; __NR_exit
		mov ebx, 0	; return value
		int 0x80	; kernel interrupt
