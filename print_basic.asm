; Copyright (c) 2012 Bartosz Szczesny
; LICENSE: The MIT License (MIT)

section .data
	msg	db "Hello World!", 10	; 10 is newline ('\n')
	msg_len	equ $ - msg		; $ means address of current position
					; substract address of 'msg' array to
					; get the number of characters inside

section .text
	global _start			; for linker (ld)
	_start:
		mov eax, 4		; __NR_write
		mov ebx, 1		; 1 is stdout, 2 is stderr
		mov ecx, msg		; pointer to the characters
		mov edx, msg_len	; length of character array
		int 0x80		; kernel interrupt

		mov eax, 1	; __NR_exit
		mov ebx, 0	; return value
		int 0x80	; kernel interrupt
