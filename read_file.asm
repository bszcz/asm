; Copyright (c) 2012 Bartosz Szczesny
; LICENSE: The MIT License (MIT)

section .data
	path	db "file.txt", 0		;  0 = "\0"
	msg	db "Reading file: file.txt", 10	; 10 = "\n"
	len	db $ - msg			; lenght = current address ("$") - address of "msg"

section .bss
	buf resb 80	; reserve 80 byte buffer, put address to "buf"

section .text
	global _start

	_start:

		; write welcome msg
		mov eax, 4		; __NR_write
		mov ebx, 1		; write to STDOUT
		mov ecx, msg		; write from "msg"
		mov edx, len		; write "len" bytes
		int 0x80		; kernel interrupt

		; read to buffer
		mov eax, 5		; __NR_open
		mov ebx, path		; open "path"
		mov ecx, 0		; O_RDONLY flag, see below
		mov edx, 0		; mode, ignored as not O_CREAT new file
		int 0x80		; kernel interrupt

		; __NR_open returns fd in eax
		xchg eax, ebx		; fd now in ebx

		; read from file to buffer
		mov eax, 3		; __NR_read
		;mov ebx...		; fd alread in ebx
		mov ecx, buf		; write to buffer
		mov edx, 80		; write 80 bytes
		int 0x80		; kernel interrupt

		; write from buffer
		mov eax, 4		; __NR_write
		mov ebx, 1		; write to STDOUT
		mov ecx, buf		; write from buffer
		mov edx, 80		; write 80 bytes
		int 0x80		; kernel interrupt

		; exit
		mov eax, 1		; __NR_exit
		mov ebx, 0		; return value
		int 0x80		; kernel interrupt

; // Copyright (c) 2012 Bartosz Szczesny
; // LICENSE: The MIT License (MIT)
;
; #include <stdio.h>
; #include <fcntl.h>
;
; int main() {
;	printf("flag     hex  dec  \n");
;	printf("------------------ \n")
;	printf("O_RDONLY %04x %04d \n", O_RDONLY, O_RDONLY);
;	printf("O_WRONLY %04x %04d \n", O_WRONLY, O_WRONLY);
;	printf("O_RDWR   %04x %04d \n", O_RDWR  , O_RDWR  );
;	printf("O_CREAT  %04x %04d \n", O_CREAT , O_CREAT );
;	printf("O_APPEND %04x %04d \n", O_APPEND, O_APPEND);
;	return 0;
; }


; flag     hex  dec
; ------------------
; O_RDONLY 0000 0000
; O_WRONLY 0001 0001
; O_RDWR   0002 0002
; O_CREAT  0040 0064
; O_APPEND 0400 1024
